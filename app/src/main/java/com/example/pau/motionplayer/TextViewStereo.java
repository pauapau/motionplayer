package com.example.pau.motionplayer;

import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by oko on 05.04.2016.
 */
public class TextViewStereo {

    TextView master;
    TextView slave;

    public void setMasterSlave(TextView master, TextView slave)
    {
        this.master=master;
        this.slave=slave;
    }

    public void setText(String str)
    {
        master.setText(str);
        slave.setText(str);
    }


}
