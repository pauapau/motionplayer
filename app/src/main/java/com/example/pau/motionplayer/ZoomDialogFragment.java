package com.example.pau.motionplayer;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

/**
 * Created by oko on 25.01.2016.
 */
public class ZoomDialogFragment extends DialogFragment {

    MainActivity mainActivity;

    static ZoomDialogFragment newInstance(MainActivity mainActivity) {
        ZoomDialogFragment f = new ZoomDialogFragment();
        f.setMainActivity(mainActivity);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater =  getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.zoom_dialog, null);

        // Created a new Dialog
        final Dialog dialog = new Dialog(mainActivity,R.style.Dialog);
        dialog.setContentView(view);

        immersiveBugFix(dialog);


        //um das immersive(fullscreen ohne menu bar oben) zu bekommen
        //ist ein hack
        //da von Android verbugt
        //jedeoch wird kein softinput mehr akzeptier fuer diesen dialog. der input wird
        //an die darunterliegende Activity weitergeleitet
        dialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

        //Button ZoomXM
        ButtonStereo btnZooMXMasterSlave=new ButtonStereo();
        Button btnZoomXMMaster = (Button)view.findViewById(R.id.btnZoomXMMaster);
        Button btnZoomXMSlave=(Button)view.findViewById(R.id.btnZoomXMSlave);

        btnZooMXMasterSlave.setMasterSlave(btnZoomXMMaster, btnZoomXMSlave);
        btnZooMXMasterSlave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainActivity.zoom(1, 0);
            }
        });

        //Button ZoomYM
        ButtonStereo btnZooYMasterSlave=new ButtonStereo();
        Button btnZoomYMMaster = (Button)view.findViewById(R.id.btnZoomYMMaster);
        Button btnZoomYMSlave=(Button)view.findViewById(R.id.btnZoomYMSlave);

        btnZooYMasterSlave.setMasterSlave(btnZoomYMMaster, btnZoomYMSlave);
        btnZooYMasterSlave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainActivity.zoom(0,1);
            }
        });


        //Button ZoomYP
        ButtonStereo btnZooYPMasterSlave=new ButtonStereo();
        Button btnZoomYPMaster = (Button)view.findViewById(R.id.btnZoomYPMaster);
        Button btnZoomYPSlave=(Button)view.findViewById(R.id.btnZoomYPSlave);

        btnZooYPMasterSlave.setMasterSlave(btnZoomYPMaster, btnZoomYPSlave);
        btnZooYPMasterSlave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainActivity.zoom(0,-1);
            }
        });


        //Button ZoomXP
        ButtonStereo btnZooXPMasterSlave=new ButtonStereo();
        Button btnZoomXPMaster = (Button)view.findViewById(R.id.btnZoomXPMaster);
        Button btnZoomXPSlave=(Button)view.findViewById(R.id.btnZoomXPSlave);

        btnZooXPMasterSlave.setMasterSlave(btnZoomXPMaster, btnZoomXPSlave);
        btnZooXPMasterSlave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mainActivity.zoom(-1,0);
            }
        });

        return dialog;
    }

    private void immersiveBugFix(final Dialog dialog)
    {
        //um das immersive(fullscreen ohne menu bar oben) zu bekommen
        //ist ein hack
        //da von Android verbugt
        //jedeoch wird kein softinput mehr akzeptier fuer diesen dialog. der input wird
        //an die darunterliegende Activity weitergeleitet
        dialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.getWindow().getDecorView().setSystemUiVisibility(setSystemUiVisibility());
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialoger) {
                //Clear the not focusable flag from the window
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
            }
        });
        dialog.getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    dialog.getWindow().getDecorView().setSystemUiVisibility(setSystemUiVisibility());
                }

            }
        });
    }

    private static int setSystemUiVisibility() {
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    }

    private void setMainActivity(MainActivity mainActivity)
    {
        this.mainActivity=mainActivity;
    }
}
