package com.example.pau.motionplayer;


import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.FrameLayout;


import com.google.vrtoolkit.cardboard.CardboardActivity;
import com.google.vrtoolkit.cardboard.CardboardView;
import com.google.vrtoolkit.cardboard.Eye;
import com.google.vrtoolkit.cardboard.HeadTransform;
import com.google.vrtoolkit.cardboard.Viewport;

import java.io.IOException;

import javax.microedition.khronos.egl.EGLConfig;



//TODO
//abspiel formate einstellen
public class MainActivity extends CardboardActivity implements CardboardView.StereoRenderer,
        SurfaceTexture.OnFrameAvailableListener, VideoControllerView.MediaPlayerControl{

    private static final int REQUEST_SAVE=2131627391;
    private static final int REQUEST_LOAD=886937840;
    private static final String TAG = "MainActivity";

    private static final int GL_TEXTURE_EXTERNAL_OES = 0x8D65;

    private int mTextureID;

    private Sphere sphere;
    private SurfaceTexture mSurface;

    private boolean updateSurface = false;
    private boolean updateTextureZoom = false;
    private boolean updateTextureZoomDefault=false;
    private boolean updateTextureZoom360=false;
    private boolean updateRecenterVideo =false;

    private int mProgram;
    private int mPositionHandle;
    private int mTextureHandle;
    private int muMVPMatrixHandle;
    private int muSTMatrixHandle;

    float mSTMatrix[]=new float[16];
    float camera[]=new float[16];
    float headView[]=new float[16];
    float view[]=new float[16];
    float mMVPMatrix[]=new float[16];
    float roateMatrix[]=new float[16];


    float scratchMatrix[]=new float[16];
    float modelMatrix[]=new float[16];
    float scratchMatrixX[]=new float[16];

    float eulerAngles[]=new float[3];

    private VideoControllerView videoControllerView;
    private MediaPlayer mMediaPlayer;
    private boolean isInit=false;
    private boolean isMediaPlayerPrepared=false;

    int zoomX=0;
    int zoomY=0;

    Double eulerXBuffer, eulerYBuffer, eulerZBuffer;
    Double eulerX=0.D , eulerY=0.D, eulerZ=0.D;

    SharedPreferences sharedPreferences;
    SettingsLoader settingsLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences=getSharedPreferences("motionplayer",MODE_PRIVATE);
        settingsLoader=new SettingsLoader(sharedPreferences);

        sphere=new Sphere(settingsLoader,36f,72f,4f,settingsLoader.loadX(),settingsLoader.loadY());

        setContentView(R.layout.activity_main);
        initCardBoardView();
        initMediaPlayer();
        initVideoControllerView();
    }


    private void initMediaPlayer()
    {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                isMediaPlayerPrepared = true;
                videoControllerView.updateButtonsActivation();
                start();
            }
        });

        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                return false;
            }
        });
    }



    private void initCardBoardView()
    {
        CardboardView cardboardView = (CardboardView) findViewById(R.id.cardboard_view);
        cardboardView.setSettingsButtonEnabled(false);
        cardboardView.setRenderer(this);
        setCardboardView(cardboardView);
    }

    private void initVideoControllerView()
    {
        videoControllerView =new VideoControllerView(this,this);
        videoControllerView.setMediaPlayer(this);
        videoControllerView.setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));
    }


    @Override
    public void onSurfaceCreated(EGLConfig config) {

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Dark background so text shows up well.

        createProgram();
        bindTexture();
        getVarByShader();
    }

    @Override
    public void onNewFrame(HeadTransform headTransform) {


        Matrix.setIdentityM(camera, 0);
        // Build the camera matrix and apply it to the ModelView.
        Matrix.setLookAtM(camera, 0, 0.0f, 0.0f, 0.01f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f);

        headTransform.getHeadView(headView, 0);

        headTransform.getEulerAngles(eulerAngles, 0);
        eulerXBuffer =Math.toDegrees(eulerAngles[0]);
        eulerYBuffer =Math.toDegrees(eulerAngles[1]);
        eulerZBuffer =Math.toDegrees(eulerAngles[2]);



        checkGlError("onReadyToDraw");
    }

    @Override
    public void onDrawEye(Eye eye) {

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        GLES20.glUseProgram(mProgram);
        checkGlError("glUseProgram");


        Matrix.setIdentityM(view, 0);
        Matrix.setIdentityM(mMVPMatrix, 0);
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.setIdentityM(roateMatrix, 0);

        synchronized(this) {
            if (updateSurface) {
                mSurface.updateTexImage();
                mSurface.getTransformMatrix(mSTMatrix);
                updateSurface = false;
            }

            if(updateTextureZoom){
                sphere.zoom(zoomX,zoomY);
                zoomX=0;
                zoomY=0;
                updateTextureZoom =false;
            }

            if(updateTextureZoomDefault)
            {
                sphere.zoomDefault();
                updateTextureZoomDefault=false;
            }

            if(updateTextureZoom360)
            {
                sphere.zoom360();
                updateTextureZoom360=false;
            }

            if(updateRecenterVideo)
            {
                eulerX=eulerXBuffer;
                eulerY=eulerYBuffer;
                eulerZ=eulerZBuffer;

                Log.d(" x: ", eulerX.toString());
                Log.d(" y: ", eulerY.toString());
                Log.d(" z: ", eulerZ.toString());

                updateRecenterVideo =false;
            }
        }





        // Apply the eye transformation to the camera.
        Matrix.multiplyMM(view, 0, eye.getEyeView(), 0, camera, 0);
        // Build the ModelView and ModelViewProjection matrices
        // for calculating cube position and light.
        float[] perspective = eye.getPerspective(1, 15);
        Matrix.multiplyMM(mMVPMatrix, 0, perspective, 0, view, 0);



        //hoch runter
        //Matrix.setRotateM(rotateMatrixX, 0, -eulerX.floatValue(), 1.f, 0.f, 0.0f);
        //Matrix.setRotateM(rotateMatrixY, 0, eulerY.floatValue(), 0.f, 1.f, 0.0f);
        //Matrix.setRotateM(rotateMatrixZ, 0, eulerZ.floatValue(), 0.f, 0.f, 1.0f);


        //damit das video beim start mittig positioniert ist


        /*float x,y,z;

        if(eulerSumme!=0)
        {
            x=eulerX.floatValue()/eulerSumme;
            y=eulerY.floatValue()/eulerSumme;
            z=eulerZ.floatValue()/eulerSumme;

            Matrix.setRotateM(roateMatrix, 0, eulerSumme,
                   x,
                    y, z);
        }
        else
        {
            Matrix.setRotateM(roateMatrix, 0, 270.f,
                    0,
                   1.f,
                    0);
        }*/

        Matrix.setRotateM(roateMatrix, 0, eulerZ.floatValue(), 0.f, 0.f, 1.0f);
        Matrix.rotateM(roateMatrix, 0, -eulerY.floatValue(), 0.f, 1.f, 0.0f);
        Matrix.rotateM(roateMatrix, 0, -eulerX.floatValue(), 1.f, 0.f, 0.0f);


        //Matrix.rotateM(roateMatrix, 0, 270, 0.f, 1.f, 0.0f);
        Matrix.multiplyMM(scratchMatrix, 0, mMVPMatrix, 0, roateMatrix, 0);


        //dreht das bild->damit nicht mehr spiegelverkehrt
        Matrix.scaleM(scratchMatrix, 0, 1.f, 1.f, -1.f);



        GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, scratchMatrix, 0);
        GLES20.glUniformMatrix4fv(muSTMatrixHandle, 1, false, mSTMatrix, 0);


        checkGlError("glVertexAttribPointer aPosition");
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        checkGlError("glEnableVertexAttribArray maPositionHandle");

        checkGlError("glVertexAttribPointer maTextureHandle");
        GLES20.glEnableVertexAttribArray(mTextureHandle);
        checkGlError("glEnableVertexAttribArray aTextureHandle");


        for (int i=0; i<sphere.getCountVerticesSize();i++)
        {
            GLES20.glVertexAttribPointer(
                    mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, sphere.getVertexBuffer(i));

            GLES20.glVertexAttribPointer(mTextureHandle, 2,
                    GLES20.GL_FLOAT, false,
                    0, sphere.getTextureBuffer(i));

            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0,sphere.getCountVertices(i));
        }

        GLES20.glDisableVertexAttribArray(mPositionHandle);
        GLES20.glDisableVertexAttribArray(mTextureHandle);

        checkGlError("glDrawArrays");
        GLES20.glFinish();

    }

    @Override
    public void onFinishFrame(Viewport viewport) {
    }

    @Override
    public void onRendererShutdown() {

    }

    @Override
    public void onSurfaceChanged(int width, int height){

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glDepthFunc(GLES20.GL_LEQUAL);
        GLES20.glDepthMask(true);

    }

    @Override
    synchronized public void onFrameAvailable(SurfaceTexture surfaceTexture)
    {
        updateSurface = true;
    }

    private void createProgram(){
        int vertexShader=OpenGLHelper.createShader(GLES20.GL_VERTEX_SHADER,
                OpenGLHelper.readRawTextFile(this, R.raw.vertex));
        int fragmentShader=OpenGLHelper.createShader(GLES20.GL_FRAGMENT_SHADER,
                OpenGLHelper.readRawTextFile(this, R.raw.fragment));

        mProgram =OpenGLHelper.createProgramm(vertexShader,fragmentShader);
    }

    private void getVarByShader()
    {
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        checkGlError("glGetAttribLocation aPosition");
        if (mPositionHandle == -1) {
            throw new RuntimeException("Could not get attrib location for aPosition");
        }
        mTextureHandle = GLES20.glGetAttribLocation(mProgram, "aTextureCoord");
        checkGlError("glGetAttribLocation aTextureCoord");
        if (mTextureHandle == -1) {
            throw new RuntimeException("Could not get attrib location for aTextureCoord");
        }

        muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        checkGlError("glGetUniformLocation uMVPMatrix");
        if (muMVPMatrixHandle == -1) {
            throw new RuntimeException("Could not get attrib location for uMVPMatrix");
        }

        muSTMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uSTMatrix");
        checkGlError("glGetUniformLocation uSTMatrix");
        if (muSTMatrixHandle == -1) {
            throw new RuntimeException("Could not get attrib location for uSTMatrix");
        }
    }

    private void bindTexture()
    {
        int[] textures = new int[1];
        GLES20.glGenTextures(1, textures, 0);

        mTextureID = textures[0];
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, mTextureID);
        checkGlError("glBindTexture mTextureID");

        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER,
                GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER,
                GLES20.GL_LINEAR);
    }

    private void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        videoControllerView.show();
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(handelHotkeysFromControllerInput(event)==true)
        {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    private boolean handelHotkeysFromControllerInput(KeyEvent event)
    {
        if(((event.isFromSource(InputDevice.SOURCE_JOYSTICK))||
                event.isFromSource(InputDevice.SOURCE_GAMEPAD))
                && KeyEvent.ACTION_UP==event.getAction())
        {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    mySeekTo(-1);
                    return true;

                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    mySeekTo(1);
                    return true;

                //um zum mediaplayer_controller view zu kommen
                case KeyEvent.KEYCODE_BUTTON_A:
                    videoControllerView.show();
                    return false;

                //pause oder player
                case KeyEvent.KEYCODE_BUTTON_START:
                    videoControllerView.doPauseResume();
                    return true;

                //recnter Video
                case KeyEvent.KEYCODE_BUTTON_X:
                    recenterVideo();
                    return true;

                //Zoom
                case KeyEvent.KEYCODE_BUTTON_R1:
                    zoom(1, 0);
                    return true;

                //Zoom
                case KeyEvent.KEYCODE_BUTTON_R2:
                    zoom(-1, 0);
                    return true;

                //Zoom
                case KeyEvent.KEYCODE_BUTTON_L1:
                    zoom(0, 1);
                    return true;

                //Zoom
                case KeyEvent.KEYCODE_BUTTON_L2:
                    zoom(0, -1);
                    return true;
            }
        }
        return false;
    }

    public synchronized void onActivityResult(final int requestCode,
                                              int resultCode, final Intent data) {

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == REQUEST_SAVE) {
                System.out.println("Saving...");
            } else if (requestCode == REQUEST_LOAD) {
                System.out.println("Loading...");
            }

            String filePath = data.getStringExtra(FileDialog.RESULT_PATH);
            initMediaPlayer(filePath);

        } else if (resultCode == Activity.RESULT_CANCELED) {
            System.out.println("canceled...");
        }

    }

    private void initMediaPlayer(String filePath)
    {
        if(mMediaPlayer!=null)
        {
            if(isInit)
            {
                isMediaPlayerPrepared=false;
                stop();
                mMediaPlayer.reset();
            }
        }

        try {
            if(filePath.isEmpty())
            {
                return;
            }
            mMediaPlayer.setDataSource(filePath);
            //afd.close();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            return;
        }

        mSurface = new SurfaceTexture(mTextureID);
        mSurface.setOnFrameAvailableListener(this);


        Surface surface = new Surface(mSurface);
        mMediaPlayer.setSurface(surface);
        mMediaPlayer.setScreenOnWhilePlaying(true);
        surface.release();

        try {
            mMediaPlayer.prepare();
        } catch (IOException t) {
            Log.e(TAG, "media player prepare failed");
        }

        synchronized(this) {
            updateSurface = false;
        }
        isInit=true;

        videoControllerView.show();
        videoControllerView.updatePausePlay();
    }

    private void mySeekTo(int i)
    {
        if(!videoControllerView.isShown()) {
            int currentPosition;
            if (mMediaPlayer.isPlaying()) {
                currentPosition = mMediaPlayer.getCurrentPosition();
                seekTo(currentPosition + 10000 * i);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_player, menu);
        return true;
    }

    @Override
    public void recenterVideo()
    {
        synchronized (this) {
            updateRecenterVideo = true;
        }
    }


    @Override
    public boolean canPause() {
        return isMediaPlayerPrepared;
    }

    @Override
    public void stop()
    {
        mMediaPlayer.stop();
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public int getCurrentPosition() {
        return mMediaPlayer.getCurrentPosition();
    }

    @Override
    public int getDuration() {
        return mMediaPlayer.getDuration();
    }

    @Override
    public boolean isPlaying() {
        return mMediaPlayer.isPlaying();
    }

    @Override
    public void pause() {
        mMediaPlayer.pause();
    }

    @Override
    public void seekTo(int i) {
        mMediaPlayer.seekTo(i);
    }

    @Override
    public void start() {
        mMediaPlayer.start();
        videoControllerView.updatePausePlay();
    }

    @Override
    public boolean isPlayerInit()
    {
        return isInit;
    }

    @Override
    public void openFileActivity()
    {
        Intent intent = new Intent(getBaseContext(), FileDialog.class);

        intent.putExtra(FileDialog.START_PATH, settingsLoader.loadDefaultVideoLocation());

        //can user select directories or not
        intent.putExtra(FileDialog.CAN_SELECT_DIR, false);

        startActivityForResult(intent, REQUEST_SAVE);
    }



    @Override
    public void ZoomDefault(){
        synchronized (this) {
            updateTextureZoomDefault = true;
        }
    }

    @Override
    public void Zoom360()
    {
        synchronized (this) {
            updateTextureZoom360 = true;
        }
    }



    @Override
    public void setOnCompletionListener()
    {

        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoControllerView.updatePausePlay();
            }
        });
    }

    @Override
    public void onDestroy()
    {
        mMediaPlayer.reset();
        super.onDestroy();
    }

    @Override
    public void onPause()
    {
        mMediaPlayer.pause();
        super.onPause();
    }

    @Override
    public void onResume()
    {
        if(isPlayerInit()) {
            videoControllerView.updatePausePlay();
        }
        super.onResume();
    }

    //x=1 --> bild wird in x richtung größer
    //x=-1 --> bild winrd in x richtugng kleiner
    //y=1 --> bild wird in y richtung größer
    //y=-1 --> bild wird in y richtung kleiner
    @Override
    public void zoom(int x, int y)
    {
        synchronized (this) {
            zoomX = x;
            zoomY = y;
            updateTextureZoom = true;
        }
    }

    @Override
    public void showZoomDialog()
    {
        DialogFragment dialogFragment=ZoomDialogFragment.newInstance(this);
        dialogFragment.show(getFragmentManager(), "zoom_dialog");
    }

    @Override
    public void showMenu()
    {
        DialogFragment dialogFragment=MenuDialogFragment.newInstance(this);
        dialogFragment.show(getFragmentManager(), "menuDialog");

        //um den immersive bug zu wurde das Flag FLAG_NOT_FOCUSABLE gesetzte
        //dadurch gehen aber alle softinput zur activty die den dialog gestart hat
        // deswegen muss es wieder aufgebhoben werden
        //removeFlagImmersiveBUG(dialogFragment);


    }

    @Override
    public void showCalibrateDialog()
    {
        DialogFragment dialogFragment= CalibrationDialogFragment.newInstance(this);
        dialogFragment.show(getFragmentManager(), "calibrateDialog");

        //um den immersive bug zu wurde das Flag FLAG_NOT_FOCUSABLE gesetzte
        //dadurch gehen aber alle softinput zur activty die den dialog gestart hat
        // deswegen muss es wieder aufgebhoben werden
        //removeFlagImmersiveBUG(dialogFragment);

    }

    //um den immersive bug zu wurde das Flag FLAG_NOT_FOCUSABLE gesetzte
    //dadurch gehen aber alle softinput zur activty die den dialog gestart hat
    // deswegen muss es wieder aufgebhoben werden
    private void removeFlagImmersiveBUG(DialogFragment dialogFragment)
    {
        dialogFragment.getDialog().getWindow().clearFlags(
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }
}
