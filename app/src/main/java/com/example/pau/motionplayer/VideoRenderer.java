package com.example.pau.motionplayer;

import android.graphics.SurfaceTexture;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.google.vrtoolkit.cardboard.CardboardActivity;
import com.google.vrtoolkit.cardboard.CardboardView;
import com.google.vrtoolkit.cardboard.Eye;
import com.google.vrtoolkit.cardboard.HeadTransform;
import com.google.vrtoolkit.cardboard.Viewport;

import javax.microedition.khronos.egl.EGLConfig;

/**
 * Created by oko on 05.01.2016.
 */
/*
public class VideoRenderer extends CardboardActivity implements CardboardView.StereoRenderer,
        SurfaceTexture.OnFrameAvailableListener{
    private static final int REQUEST_SAVE=2131627391;
    private static final int REQUEST_LOAD=886937840;
    private static final String TAG = "MainActivity";

    private static final int GL_TEXTURE_EXTERNAL_OES = 0x8D65;

    private int mTextureID;

    private Sphere sphere;
    private SurfaceTexture mSurface;

    private boolean updateSurface = false;

    private int mProgram;
    private int mPositionHandle;
    private int mTextureHandle;
    private int muMVPMatrixHandle;
    private int muSTMatrixHandle;


    float mProjectionMatrix[]=new float[16];
    float mSTMatrix[]=new float[16];
    float camera[]=new float[16];
    float headView[]=new float[16];
    float view[]=new float[16];
    float canvas[]=new float[16];
    float mMVPMatrix[]=new float[16];



    public VideoRenderer(Float stepsZ, Float stepsBezugsgrade, float radius, int begrenzungZ, int begrenzungX)
    {
        //sphere=new Sphere(stepsZ,stepsBezugsgrade,radius,begrenzungZ,begrenzungX);
    }

    public void newSphereSize(int begrenzungZ, int begrenzungX)
    {
       // sphere.newSphereSize(begrenzungZ,begrenzungX);
    }

    @Override
    public void onSurfaceCreated(EGLConfig config) {

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Dark background so text shows up well.

        createProgram();
        bindTexture();
        getVarByShader();

        /*
        mSurface = new SurfaceTexture(mTextureID);
        mSurface.setOnFrameAvailableListener(this);


        Surface surface = new Surface(mSurface);
        mMediaPlayer.setSurface(surface);
        mMediaPlayer.setScreenOnWhilePlaying(true);
        surface.release();

        try {
            mMediaPlayer.prepare();
        } catch (IOException t) {
            Log.e(TAG, "media player prepare failed");
        }

        synchronized(this) {
            updateSurface = false;
        }

        videoControllerView.setMediaPlayer(this);
        videoControllerView.setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));
        videoControllerView.show();

        start();
        videoControllerView.updatePausePlay();
    }

    @Override
    public void onNewFrame(HeadTransform headTransform) {

        Matrix.setIdentityM(camera, 0);
        // Build the camera matrix and apply it to the ModelView.
        Matrix.setLookAtM(camera, 0, 0.0f, 0.0f, 0.01f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f);

        headTransform.getHeadView(headView, 0);

        checkGlError("onReadyToDraw");
    }

    @Override
    public void onDrawEye(Eye eye) {

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        GLES20.glUseProgram(mProgram);
        checkGlError("glUseProgram");


        Matrix.setIdentityM(view,0);
        Matrix.setIdentityM(mMVPMatrix,0);

        // Apply the eye transformation to the camera.
        Matrix.multiplyMM(view, 0, eye.getEyeView(), 0, camera, 0);

        // Set the position of the light
        //Matrix.multiplyMV(lightPosInEyeSpace, 0, view, 0, LIGHT_POS_IN_WORLD_SPACE, 0);

        // Build the ModelView and ModelViewProjection matrices
        // for calculating cube position and light.
        float[] perspective = eye.getPerspective(1, 15);
        // Matrix.multiplyMM(modelView, 0, view, 0, modelCube, 0);
        Matrix.multiplyMM(mMVPMatrix, 0, perspective, 0, view, 0);
        //drawCube();










        synchronized(this) {
            if (updateSurface) {
                mSurface.updateTexImage();
                mSurface.getTransformMatrix(mSTMatrix);
                updateSurface = false;
            }
        }

        GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, mMVPMatrix, 0);
        GLES20.glUniformMatrix4fv(muSTMatrixHandle, 1, false, mSTMatrix, 0);


        checkGlError("glVertexAttribPointer aPosition");
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        checkGlError("glEnableVertexAttribArray maPositionHandle");

        checkGlError("glVertexAttribPointer maTextureHandle");
        GLES20.glEnableVertexAttribArray(mTextureHandle);
        checkGlError("glEnableVertexAttribArray aTextureHandle");

/*
        for(int i=0; i<sphere.getVertexBuffer().size();i++)
        {
            GLES20.glVertexAttribPointer(
                    mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, sphere.getVertexBuffer().get(i));

            GLES20.glVertexAttribPointer(mTextureHandle, 2,
                    GLES20.GL_FLOAT, false,
                    0, sphere.getTextureBuffer().get(i));

            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0,sphere.getCountVertices(i));
        }*/


/*
        for (int i = 0; i < this.sphere.getTotalNumStrips(); i++) {
            GLES20.glVertexAttribPointer(
                    mPositionHandle, sphere.getVertexNumberPerStrip(),
                    GLES20.GL_FLOAT, false,
                    0, sphere.getVertexBuffer().get(i));

            GLES20.glVertexAttribPointer(mTextureHandle, 2, GLES20.GL_FLOAT, false,
                    0, sphere.getTextureBuffer().get(i));

            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, sphere.getmVertices().get(i).length / sphere.getAmountNumbersPerVertices());
        }


        GLES20.glDisableVertexAttribArray(mPositionHandle);
        GLES20.glDisableVertexAttribArray(mTextureHandle);

        checkGlError("glDrawArrays");
        GLES20.glFinish();

    }

    @Override
    public void onFinishFrame(Viewport viewport) {
    }

    @Override
    public void onRendererShutdown() {

    }

    @Override
    public void onSurfaceChanged(int width, int height){

        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glDepthFunc(GLES20.GL_LEQUAL);
        GLES20.glDepthMask(true);

    }

    @Override
    synchronized public void onFrameAvailable(SurfaceTexture surfaceTexture)
    {
        updateSurface = true;
    }

    private void createProgram(){
        int vertexShader=OpenGLHelper.createShader(GLES20.GL_VERTEX_SHADER,
                OpenGLHelper.readRawTextFile(this, R.raw.vertex));
        int fragmentShader=OpenGLHelper.createShader(GLES20.GL_FRAGMENT_SHADER,
                OpenGLHelper.readRawTextFile(this, R.raw.fragment));

        mProgram =OpenGLHelper.createProgramm(vertexShader,fragmentShader);
    }

    private void getVarByShader()
    {
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
        checkGlError("glGetAttribLocation aPosition");
        if (mPositionHandle == -1) {
            throw new RuntimeException("Could not get attrib location for aPosition");
        }
        mTextureHandle = GLES20.glGetAttribLocation(mProgram, "aTextureCoord");
        checkGlError("glGetAttribLocation aTextureCoord");
        if (mTextureHandle == -1) {
            throw new RuntimeException("Could not get attrib location for aTextureCoord");
        }

        muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        checkGlError("glGetUniformLocation uMVPMatrix");
        if (muMVPMatrixHandle == -1) {
            throw new RuntimeException("Could not get attrib location for uMVPMatrix");
        }

        muSTMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uSTMatrix");
        checkGlError("glGetUniformLocation uSTMatrix");
        if (muSTMatrixHandle == -1) {
            throw new RuntimeException("Could not get attrib location for uSTMatrix");
        }
    }

    private void bindTexture()
    {
        int[] textures = new int[1];
        GLES20.glGenTextures(1, textures, 0);

        mTextureID = textures[0];
        GLES20.glBindTexture(GL_TEXTURE_EXTERNAL_OES, mTextureID);
        checkGlError("glBindTexture mTextureID");

        //GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_S,GLES20.GL_REPEAT);
        //GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,GLES20.GL_TEXTURE_WRAP_T,GLES20.GL_REPEAT);


        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER,
                GLES20.GL_NEAREST);
        GLES20.glTexParameterf(GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER,
                GLES20.GL_LINEAR);
    }

    private void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }

/*
    public int getBegrenzungX()
    {
        return sphere.getBegrenzungX();
    }

    public Float getStepsZ()
    {
        return sphere.getStepsZ();
    }

    public Float getStepsBezugsgrade()
    {
        return sphere.getStepsBezugsgrade();
    }

    public float getRadius()
    {
        return sphere.getRadius();
    }

    public int getBegrenzungZ()
    {
        return sphere.getBegrenzungZ();
    }






    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }



}
*/