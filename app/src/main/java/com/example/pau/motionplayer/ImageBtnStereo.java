package com.example.pau.motionplayer;

import android.view.View;
import android.widget.ImageButton;

/**
 * Created by oko on 04.04.2016.
 */
public class ImageBtnStereo {
    ImageButton master;
    ImageButton slave;

    public void setMasterSlave(ImageButton master, ImageButton slave)
    {
        this.master=master;
        this.slave=slave;
    }

    public void setOnClickListener(View.OnClickListener mPauseListener)
    {
        master.setOnClickListener(mPauseListener);
        slave.setOnClickListener(mPauseListener);
    }

    public void setOnFocusChangeListener (View.OnFocusChangeListener onFocusChangeListener)
    {
        master.setOnFocusChangeListener(onFocusChangeListener);
        slave.setOnFocusChangeListener(onFocusChangeListener);
    }

    public void setEnabled(boolean enabled)
    {
        if(master!=null) {
            master.setEnabled(enabled);
        }
        if(slave!=null)
        {
            slave.setEnabled(enabled);
        }
    }

    public boolean isFocused()
    {
        if(master.isFocused()||slave.isFocused())
        {
            return true;
        }
        return false;
    }

    public void setImageResource(int id)
    {
        master.setImageResource(id);
        slave.setImageResource(id);
    }

}
