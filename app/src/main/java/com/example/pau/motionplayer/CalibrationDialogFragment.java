package com.example.pau.motionplayer;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

/**
 * Created by oko on 07.04.2016.
 */
public class CalibrationDialogFragment extends DialogFragment {
    private MainActivity mainActivity;

    static CalibrationDialogFragment newInstance(MainActivity mainActivity) {
        CalibrationDialogFragment f = new CalibrationDialogFragment();
        f.setMainActivity(mainActivity);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater =  getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.calibration_dialog, null);

        // Created a new Dialog
        final Dialog dialog = new Dialog(mainActivity,R.style.Dialog);
        dialog.setContentView(view);

        immersiveBugFix(dialog);


        return dialog;
    }

    private void setMainActivity(MainActivity mainActivity)
    {
        this.mainActivity=mainActivity;
    }


    private void immersiveBugFix(final Dialog dialog)
    {
        //um das immersive(fullscreen ohne menu bar oben) zu bekommen
        //ist ein hack
        //da von Android verbugt
        //jedeoch wird kein softinput mehr akzeptier fuer diesen dialog. der input wird
        //an die darunterliegende Activity weitergeleitet
        dialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.getWindow().getDecorView().setSystemUiVisibility(setSystemUiVisibility());
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialoger) {
                //Clear the not focusable flag from the window
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
            }
        });
        dialog.getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    dialog.getWindow().getDecorView().setSystemUiVisibility(setSystemUiVisibility());
                }

            }
        });
    }

    private static int setSystemUiVisibility() {
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    }
}
