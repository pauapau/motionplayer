package com.example.pau.motionplayer;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ksad on 10.07.2015.
 */
public class Sphere {
    private List<FloatBuffer> mVertexBuffer = new ArrayList<>();
    private List<FloatBuffer> mTextureBuffer = new ArrayList<>();

    private int begrenzungZ;
    private int begrenzungX;
    private Float stepsZ, stepsBezugsgrade;
    private float radius;

    private ArrayList<Integer> countVertices;

    private static final int ZOOM=10;

    private SettingsLoader settingsLoader;

    public Sphere(SettingsLoader settingsLoader,Float stepsZ, Float stepsBezugsgrade, float radius, int begrenzungX,int begrenzungZ)
    {
        this.settingsLoader=settingsLoader;
        this.stepsZ=stepsZ;
        this.stepsBezugsgrade=stepsBezugsgrade;
        this.radius=radius;
        this.begrenzungX=begrenzungX;
        this.begrenzungZ=begrenzungZ;

        createSphere();
    }

    public void newSphereSize( int begrenzungX,int begrenzungZ)
    {
        this.begrenzungX=begrenzungX;
        this.begrenzungZ=begrenzungZ;

        createSphere();
    }


    private void createSphere()
    {
        Float x1,y1,z1;
        Float x2,y2,z2;
        Float bezugsgradeZ2, bezugsgradeZ1;
        Float xTextureZ2,yTextutueZ2, xTextureZ1, yTextutueZ1;

        Float stepAngleZ;
        Float stepAngleBezugsgrade;

        //calculate stepAngle
        stepAngleZ=180/stepsZ;
        stepAngleBezugsgrade=360/stepsBezugsgrade;

        countVertices=new ArrayList<>();
        mVertexBuffer=new ArrayList<>();
        mTextureBuffer=new ArrayList<>();


        for (float angleZ = 0.0f; angleZ+stepAngleZ <=180.0f; angleZ += stepAngleZ) {

            ArrayList<Float> floatCounterVertices=new ArrayList<>();
            ArrayList<Float> floatCounterTexture=new ArrayList<>();

            z1 = (float)Math.cos(Math.toRadians(angleZ) )* radius;
            bezugsgradeZ1 = (float)Math.sin(Math.toRadians(angleZ)) * radius;

            z2 = (float)Math.cos((Math.toRadians(angleZ + stepAngleZ) ) )* radius;
            bezugsgradeZ2 = (float)Math.sin((Math.toRadians(angleZ + stepAngleZ)) )*radius;

            // Fixed latitude, 360 degrees rotation to traverse a weft
            //for (float angleBezugsgrade = 0.0f; angleBezugsgrade <= 360.0f; angleBezugsgrade += stepAngleBezugsgrade) {
            for (float angleBezugsgrade = -90.0f; angleBezugsgrade <= 270.0f; angleBezugsgrade += stepAngleBezugsgrade) {

                    //Texture berechnet
                //y hoehe(180 degree=nordpol->suedpol)
                //y hoehe (360 degree=nordpol->suedpole->nordpole)
                //x breite (einmal rum)
                //Texture
                if(isPointInRange(angleBezugsgrade, angleZ, angleZ + stepAngleZ)) {
                    //Punkt2 berechen
                    x2 = calcVertexCos(angleBezugsgrade, bezugsgradeZ2);
                    y2 = calcVertexSin(angleBezugsgrade, bezugsgradeZ2);

                    //Punkt2 berechen
                    x1 = calcVertexCos(angleBezugsgrade, bezugsgradeZ1);
                    y1 = calcVertexSin(angleBezugsgrade, bezugsgradeZ1);

                    //Texture 2 berechnen
                    xTextureZ2 = calcTextureX(angleBezugsgrade);
                    yTextutueZ2 = calcTextureY(angleZ + stepAngleZ);

                    //Texture 1 berechnen
                    xTextureZ1 = xTextureZ2;
                    yTextutueZ1 = calcTextureY(angleZ);

                    //punkte 2 ins ARRay übergeben
                    floatCounterVertices.add(x2);
                    floatCounterVertices.add(z2);
                    floatCounterVertices.add(y2);

                    floatCounterTexture.add(xTextureZ2);
                    floatCounterTexture.add(yTextutueZ2);

                    //punkte 1 ins ARRay übergeben
                    floatCounterVertices.add(x1);
                    floatCounterVertices.add(z1);
                    floatCounterVertices.add(y1);

                    floatCounterTexture.add(xTextureZ1);
                    floatCounterTexture.add(yTextutueZ1);
                }
            }

            //wenn keine vertices berechnet wurden mussen auch keine hinzugefuegt werden
            if(floatCounterVertices.size()>0) {

                final float[] vertices = new float[floatCounterVertices.size()];
                final float[] texture = new float[floatCounterTexture.size()];

                //copy floats into floatArray[]
                for (int i = 0; i < floatCounterVertices.size(); i++) {
                    vertices[i] = floatCounterVertices.get(i);
                }

                for (int i = 0; i < floatCounterTexture.size(); i++) {
                    texture[i] = floatCounterTexture.get(i);
                }

                //x,y,z= 1 punkt auf dem kreis
                //countVertices braucht nur die anzahl der punkte auf dem kreis
                countVertices.add(floatCounterVertices.size()/3);

                FloatBuffer floatBufferVertex = OpenGLHelper.createFloatBuffer(floatCounterVertices.size()* Float.SIZE);
                floatBufferVertex.put(vertices);
                floatBufferVertex.position(0);
                this.mVertexBuffer.add(floatBufferVertex);

                FloatBuffer floatBufferTexture = OpenGLHelper.createFloatBuffer(floatCounterVertices.size()* Float.SIZE);
                floatBufferTexture.put(texture);
                floatBufferTexture.position(0);
                this.mTextureBuffer.add(floatBufferTexture);
            }
        }
    }

    private float calcVertexCos(float angleBezugsgrade,float bezugsgrade)
    {
        float vertex;
        float punkt;

        punkt = (float) Math.cos(Math.toRadians(angleBezugsgrade));
        vertex = punkt * bezugsgrade;

        return vertex;
    }

    private float calcVertexSin(float angleBezugsgrade,float bezugsgrade)
    {
        float vertex;
        float punkt;

        punkt = (float) Math.sin(Math.toRadians(angleBezugsgrade));
        vertex = punkt * bezugsgrade;

        return vertex;
    }
/*
    private boolean isPointInRange(float angleX1, float angleZ1, float angleZ2)
    {
        //Texture
        //y hoehe(180 degree=nordpol->suedpol)
        //y hoehe (360 degree=nordpol->suedpole->nordpole)
        //x breite (einmal rum)
        return (angleX1>=(begrenzungX/2) && angleX1<=360-(begrenzungX/2)
                && angleZ1>=(begrenzungZ/2) && angleZ1<=180-(begrenzungZ/2)
                && angleZ2>=(begrenzungZ/2) && angleZ2<=180-(begrenzungZ/2));

    }
*/
    private boolean isPointInRange(float angleX1, float angleZ1, float angleZ2)
    {
        //Texture
        //y hoehe(180 degree=nordpol->suedpol)
        //y hoehe (360 degree=nordpol->suedpole->nordpole)
        //x breite (einmal rum)
        return (angleX1>=-90+(begrenzungX/2) && angleX1<=270-(begrenzungX/2)
                && angleZ1>=(begrenzungZ/2) && angleZ1<=180-(begrenzungZ/2)
                && angleZ2>=(begrenzungZ/2) && angleZ2<=180-(begrenzungZ/2));

    }

    private float calcTextureX(float angleBezugsgrade)
    {
        float textureX;

        //Texture
        //y hoehe(180 degree=nordpol->suedpol)
        //y hoehe (360 degree=nordpol->suedpole->nordpole)
        //x breite (einmal rum)
        textureX = (((angleBezugsgrade+90-begrenzungX/2)) / (360-begrenzungX));

        return textureX;
    }

    private float calcTextureY(float angleZ)
    {
        float textureY;
        //Texture
        //y hoehe(180 degree=nordpol->suedpol)
        //y hoehe (360 degree=nordpol->suedpole->nordpole)
        //x breite (einmal rum)
        textureY = (((angleZ-begrenzungZ/2)) / (180-begrenzungZ));
        return textureY;
    }

    public void zoom(int x, int y)
    {
        int bufferBegrenzungZ=begrenzungZ+ZOOM*y;
        int bufferBegrenzungX=begrenzungX+ZOOM*x;

        if(bufferBegrenzungZ<=180 && bufferBegrenzungZ>=0
                && bufferBegrenzungX<=360 && bufferBegrenzungX>=0)
        {
            settingsLoader.saveBegrenzungXY(bufferBegrenzungX, bufferBegrenzungZ);
            newSphereSize(bufferBegrenzungX, bufferBegrenzungZ);
        }
    }

    public void zoom360()
    {
        newSphereSize(0,0);
    }

    public void zoomDefault()
    {
        newSphereSize(settingsLoader.loadX(),settingsLoader.loadY());
    }

    public int getCountVertices(int pos)
    {
        return countVertices.get(pos);
    }

    public int getCountVerticesSize()
    {
        return countVertices.size();
    }

    public FloatBuffer getVertexBuffer(int pos)
    {
        return mVertexBuffer.get(pos);
    }

    public FloatBuffer getTextureBuffer(int pos)
    {
        return mTextureBuffer.get(pos);
    }

}
