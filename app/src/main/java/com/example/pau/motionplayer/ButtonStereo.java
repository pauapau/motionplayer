package com.example.pau.motionplayer;

import android.view.View;
import android.widget.Button;

/**
 * Created by oko on 05.04.2016.
 */
public class ButtonStereo {

    Button master;
    Button slave;

    public void setMasterSlave(Button master, Button slave)
    {
        this.master=master;
        this.slave=slave;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.master.setOnClickListener(onClickListener);
        this.slave.setOnClickListener(onClickListener);
    }

}
