package com.example.pau.motionplayer;

import android.widget.CompoundButton;
import android.widget.Switch;

/**
 * Created by oko on 05.04.2016.
 */
public class SwitchStereo {
    Switch master;
    Switch slave;

    public void setMasterSlave(Switch master, Switch slave)
    {
        this.master=master;
        this.slave=slave;
    }

    public void setOnCheckedChangeListenerMaster(CompoundButton.OnCheckedChangeListener onCheckedChangeListener)
    {
        master.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    public void setOnCheckedChangeListenerSlave(CompoundButton.OnCheckedChangeListener onCheckedChangeListener)
    {
        slave.setOnCheckedChangeListener(onCheckedChangeListener);
    }
}
