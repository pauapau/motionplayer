package com.example.pau.motionplayer;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

/**
 * Created by oko on 05.04.2016.
 */
public class MenuDialogFragment extends DialogFragment {
    private MainActivity mainActivity;

     static MenuDialogFragment newInstance(MainActivity mainActivity) {

        MenuDialogFragment f = new MenuDialogFragment();
        f.setMainActivity(mainActivity);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //hideSystemUI();
        LayoutInflater inflater =  getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.menu_dialog, null);

        // Created a new Dialog
        final Dialog  dialog = new Dialog(mainActivity,R.style.Dialog);
        dialog.setContentView(view);

        immersiveBugFix(dialog);

        //Button OPen File
        Button btnOpenFileMaster=(Button) view.findViewById(R.id.btnMenuOpenFileMaster);
        Button btnOpenFileSlave=(Button) view.findViewById(R.id.btnMenuOpenFileSlave);

        ButtonStereo btnOpenFileMasterSlave=new ButtonStereo();
        btnOpenFileMasterSlave.setMasterSlave(btnOpenFileMaster, btnOpenFileSlave);

        View.OnClickListener onClickListenerOpenFile=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.openFileActivity();
                dismiss();

            }
        };

        btnOpenFileMasterSlave.setOnClickListener(onClickListenerOpenFile);


        //Button Open Zoom-Dialog
        ButtonStereo btnZoomMasterSlave=new ButtonStereo();
        Button btnZoomMaster=(Button)view.findViewById(R.id.btnMenuZoomMaster);
        Button btnZoomSlave=(Button)view.findViewById(R.id.btnMenuZoomSlave);

        btnZoomMasterSlave.setMasterSlave(btnZoomMaster,btnZoomSlave);

        View.OnClickListener onClickListenerZoom=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showZoomDialog();
                dismiss();

            }
        };

        btnZoomMasterSlave.setOnClickListener(onClickListenerZoom);


        //Button Calibrate
        ButtonStereo btnCalibrateMasterSlave = new ButtonStereo();
        Button btnCalibrateMaster = (Button) view.findViewById(R.id.btnCalibrationMaster);
        Button btnCalibrateSlave=(Button) view.findViewById(R.id.btnCalibrationSlave);

        btnCalibrateMasterSlave.setMasterSlave(btnCalibrateMaster, btnCalibrateSlave);
        btnCalibrateMasterSlave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showCalibrateDialog();
                dismiss();
            }
        });

        return dialog;
    }



    @Override
    public void show(FragmentManager fragmentManager, String tag)
    {
        super.show(fragmentManager, tag);
    }

    private void immersiveBugFix(final Dialog dialog)
    {
        //um das immersive(fullscreen ohne menu bar oben) zu bekommen
        //ist ein hack
        //da von Android verbugt
        //jedeoch wird kein softinput mehr akzeptier fuer diesen dialog. der input wird
        //an die darunterliegende Activity weitergeleitet
        dialog.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        dialog.getWindow().getDecorView().setSystemUiVisibility(setSystemUiVisibility());
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialoger) {
                //Clear the not focusable flag from the window
                dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
            }
        });
        dialog.getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    dialog.getWindow().getDecorView().setSystemUiVisibility(setSystemUiVisibility());
                }

            }
        });
    }

    private static int setSystemUiVisibility() {
        return View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    }

    private void setMainActivity(MainActivity mainActivity)
    {
        this.mainActivity=mainActivity;
    }
}
