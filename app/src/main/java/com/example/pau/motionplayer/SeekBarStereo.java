package com.example.pau.motionplayer;

import android.widget.SeekBar;

/**
 * Created by oko on 05.04.2016.
 */
public class SeekBarStereo {
    SeekBar master;
    SeekBar slave;

    public void setMasterSlave(SeekBar master, SeekBar slave)
    {
        this.master=master;
        this.slave=slave;
    }

    public void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener onSeekBarChangeListener)
    {
        master.setOnSeekBarChangeListener(onSeekBarChangeListener);
        slave.setOnSeekBarChangeListener(onSeekBarChangeListener);
    }

    public void setMax(int i)
    {
        master.setMax(i);
        slave.setMax(i);
    }

    public void setEnabled(boolean enabled)
    {
        if(master!=null) {
            master.setEnabled(enabled);
        }
        if(slave!=null)
        {
            slave.setEnabled(enabled);
        }
    }

    public void setProgress(int i)
    {
        master.setProgress(i);
        slave.setProgress(i);
    }

    public void setSecondaryProgress(int i)
    {
        master.setSecondaryProgress(i);
        slave.setSecondaryProgress(i);
    }
}
