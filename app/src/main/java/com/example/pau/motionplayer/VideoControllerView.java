package com.example.pau.motionplayer;

/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.Formatter;
import java.util.Locale;

/**
 * A view containing controls for a MediaPlayer. Typically contains the
 * buttons like "Play/Pause", "Rewind", "Fast Forward" and a progress
 * slider. It takes care of synchronizing the controls with the state
 * of the MediaPlayer.
 * <p>
 * The way to use this class is to instantiate it programatically.
 * The MediaController will create a default set of controls
 * and put them in a window floating above your application. Specifically,
 * the controls will float above the view specified with setAnchorView().
 * The window will disappear if left idle for three seconds and reappear
 * when the user touches the anchor view.
 * <p>
 * Functions like show() and hide() have no effect when MediaController
 * is created in an xml layout.
 *
 * MediaController will hide and
 * show the buttons according to these rules:
 * <ul>
 * <li> The "previous" and "next" buttons are hidden until setPrevNextListeners()
 *   has been called
 * <li> The "previous" and "next" buttons are visible but disabled if
 *   setPrevNextListeners() was called with null listeners
 * <li> The "rewind" and "fastforward" buttons are shown unless requested
 *   otherwise by using the MediaController(Context, boolean) constructor
 *   with the boolean set to false
 * </ul>
 */
public class VideoControllerView extends FrameLayout {
    private static final String TAG = "VideoControllerView";

    private MediaPlayerControl mediaPlayerControl;
    private Context             mContext;
    private ViewGroup           mAnchor;
    private View                mRoot;
    private boolean             mShowing;
    private boolean             mDragging;
    private static final int    sDefaultTimeout = 3000;
    private static final int    FADE_OUT = 1;
    private static final int    SHOW_PROGRESS = 2;
    StringBuilder               mFormatBuilder;
    Formatter                   mFormatter;



    private ImageBtnStereo btnPlayPause = new ImageBtnStereo();
    private SeekBarStereo       seekBarMasterSlave=new SeekBarStereo();
    private TextViewStereo      mEndTimeMasterSlave=new TextViewStereo();
    private TextViewStereo      mCurrentTimeMasterSlave=new TextViewStereo();


    private Handler             mHandler = new MessageHandler(this);


    private Activity activity;

    public VideoControllerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRoot = null;
        mContext = context;


        Log.i(TAG, TAG);
    }

    public VideoControllerView(Context context) {
        super(context);
        mContext = context;

        Log.i(TAG, TAG);
    }

    public VideoControllerView(Context context, Activity activity) {
        this(context);
        this.activity=activity;

        Log.i(TAG, TAG);
    }

    public void setMediaPlayer(MediaPlayerControl player) {
        mediaPlayerControl = player;
        updatePausePlay();
        mediaPlayerControl.setOnCompletionListener();
        //updateFullScreen();
    }

    /**
     * Set the view that acts as the anchor for the control view.
     * This can for example be a VideoView, or your Activity's main view.
     * @param view The view to which to anchor the controller when it is visible.
     */
    public void setAnchorView(ViewGroup view) {
        mAnchor = view;

        FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );

        removeAllViews();
        View v = makeControllerView();
        addView(v, frameParams);
    }

    /**
     * Create the view that holds the widgets that control playback.
     * Derived classes can override this to create their own.
     * @return The controller view.
     *
     */
    protected View makeControllerView() {
        LayoutInflater inflate = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRoot = inflate.inflate(R.layout.media_controller, null);

        initControllerView(mRoot);

        return mRoot;
    }

    private void initControllerView(View v) {
        final Switch mSwitchMaster, mSwitchSlave;

        SeekBar seekBarMaster;
        SeekBar seekBarSlave;

        ImageButton btnMenuMaster;
        ImageButton btnMenuSlave;
        ImageBtnStereo btnMenuMasterSlave=new ImageBtnStereo();

        ImageButton btnCenterSlave;
        ImageButton btnCenterMaster;
        ImageBtnStereo btnCenterMasterSlave=new ImageBtnStereo();

        ImageButton btnPlayPauseMaster;
        ImageButton btnPlayPauseSlave;

        TextView mEndTimeMaster, mEndTimeSlave;
        TextView mCurrentTimeMaster, mCurrentTimeSlave;

        btnPlayPauseMaster = (ImageButton) v.findViewById(R.id.btnPauseMaster);
        btnPlayPauseSlave = (ImageButton) v.findViewById(R.id.btnPauseSlave);

        SwitchStereo switchStereo=new SwitchStereo();


        btnPlayPause.setMasterSlave(btnPlayPauseMaster, btnPlayPauseSlave);
        btnPlayPause.setOnClickListener(mPauseListener);
        btnPlayPause.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                    updatePausePlay();
            }
        });

        btnCenterMaster=(ImageButton) v.findViewById(R.id.btnCenterMaster);
        btnCenterSlave=(ImageButton) v.findViewById(R.id.btnCenterSlaver);

        btnCenterMasterSlave.setMasterSlave(btnCenterMaster,btnCenterSlave);
        if(btnCenterMasterSlave!=null)
        {
            btnCenterMasterSlave.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mediaPlayerControl.recenterVideo();
                }
            });
        }

        btnMenuMaster = (ImageButton)v.findViewById(R.id.btnMenuMaster);
        btnMenuSlave = (ImageButton)v.findViewById(R.id.btnMenuSlave);
        btnMenuMasterSlave.setMasterSlave(btnMenuMaster,btnMenuSlave);
        if(btnMenuMasterSlave !=null)
        {
            btnMenuMasterSlave.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    hide();
                    mediaPlayerControl.showMenu();
                }
            });
        }


        mSwitchMaster = (Switch)v.findViewById(R.id.mSwitchMaster);
        mSwitchSlave = (Switch)v.findViewById(R.id.mSwitchSlave);

        switchStereo.setMasterSlave(mSwitchMaster, mSwitchSlave);


        if(switchStereo !=null)
        {
            switchStereo.setOnCheckedChangeListenerMaster(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {

                    if (isChecked) {
                        mediaPlayerControl.Zoom360();
                        mSwitchSlave.setChecked(isChecked);
                    } else {
                        mediaPlayerControl.ZoomDefault();
                        mSwitchSlave.setChecked(isChecked);
                    }

                }
            });

            switchStereo.setOnCheckedChangeListenerSlave(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {

                    if (isChecked) {
                        mSwitchMaster.setChecked(isChecked);
                    } else {
                        mSwitchMaster.setChecked(isChecked);
                    }

                }
            });
        }

        seekBarMaster = (SeekBar) v.findViewById(R.id.mediacontroller_progressMaster);
        seekBarSlave = (SeekBar) v.findViewById(R.id.mediacontroller_progressSlave);
        seekBarMasterSlave.setMasterSlave(seekBarMaster,seekBarSlave);

        if (seekBarMasterSlave != null) {
            seekBarMasterSlave.setOnSeekBarChangeListener(mSeekListener);
            seekBarMasterSlave.setMax(1000);
        }

        mEndTimeMaster = (TextView) v.findViewById(R.id.timeMaster);
        mCurrentTimeMaster = (TextView) v.findViewById(R.id.time_currentMaster);
        mEndTimeSlave = (TextView) v.findViewById(R.id.timeSlave);
        mCurrentTimeSlave = (TextView) v.findViewById(R.id.time_currentSlave);

        mEndTimeMasterSlave.setMasterSlave(mEndTimeMaster, mEndTimeSlave);
        mCurrentTimeMasterSlave.setMasterSlave(mCurrentTimeMaster, mCurrentTimeSlave);


        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
    }



    /**
     * Show the controller on screen. It will go away
     * automatically after 3 seconds of inactivity.
     */
    public void show() {
        show(sDefaultTimeout);
    }

    /**
     * Disable pause or seek buttons if the stream cannot be paused or seeked.
     * This requires the control interface to be a MediaPlayerControlExt
     */
    public void updateButtonsActivation() {
        if (mediaPlayerControl == null ) {
            return;
        }

        try
        {
            if(btnPlayPause != null) {
                if (!mediaPlayerControl.canPause()) {
                    btnPlayPause.setEnabled(false);
                }
                else{
                    btnPlayPause.setEnabled(true);
                }
            }

            if(seekBarMasterSlave != null)
            {
                if (!mediaPlayerControl.canPause()) {
                    seekBarMasterSlave.setEnabled(false);
                }
                else
                {
                    seekBarMasterSlave.setEnabled(true);
                }
            }
        } catch (IncompatibleClassChangeError ex) {
            // We were given an old version of the interface, that doesn't have
            // the canPause/canSeekXYZ methods. This is OK, it just means we
            // assume the media can be paused and seeked, and so we don't disable
            // the buttons.
        }
    }

    /**
     * Show the controller on screen. It will go away
     * automatically after 'timeout' milliseconds of inactivity.
     * @param timeout The timeout in milliseconds. Use 0 to show
     * the controller until hide() is called.
     */
    public void show(int timeout) {
        updatePausePlay();
        if (!mShowing && mAnchor != null) {
            setProgress();
            /*if (btnPlayPauseMaster != null) {
                btnPlayPauseMaster.requestFocus();
            }*/
            updateButtonsActivation();

            final FrameLayout.LayoutParams tlp = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM
            );
            final VideoControllerView meiner=this;

            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAnchor.addView(meiner, tlp);
                }
            });
            mShowing = true;
        }


        // cause the progress bar to be updated even if mShowing
        // was already true.  This happens, for example, if we're
        // paused with the progress bar showing the user hits play.
        mHandler.sendEmptyMessage(SHOW_PROGRESS);

        Message msg = mHandler.obtainMessage(FADE_OUT);
        if (timeout != 0) {
            mHandler.removeMessages(FADE_OUT);
            mHandler.sendMessageDelayed(msg, timeout);
        }
        else
        {
            mHandler.removeMessages(FADE_OUT);
        }
    }



    /**
     * Remove the controller from the screen.
     */
    public void hide() {
        if (mAnchor == null) {
            return;
        }

        try {
            mAnchor.removeView(this);
            mHandler.removeMessages(SHOW_PROGRESS);
        } catch (IllegalArgumentException ex) {
            Log.w("MediaController", "already removed");
        }
        mShowing = false;
    }

    private String stringForTime(int timeMs) {
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours   = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    private int setProgress() {

        if (mediaPlayerControl == null || mDragging ) {
            return 0;
        }


        if(!mediaPlayerControl.isPlayerInit())
        {
            mEndTimeMasterSlave.setText(stringForTime(0));
            mCurrentTimeMasterSlave.setText(stringForTime(0));
            return 0;
        }

        int position = mediaPlayerControl.getCurrentPosition();
        int duration = mediaPlayerControl.getDuration();
        if (seekBarMasterSlave != null) {
            if (duration > 0) {
                // use long to avoid overflow
                long pos = 1000L * position / duration;
                seekBarMasterSlave.setProgress( (int) pos);
            }
            int percent = mediaPlayerControl.getBufferPercentage();
            seekBarMasterSlave.setSecondaryProgress(percent * 10);
        }

        if (mEndTimeMasterSlave != null)
            mEndTimeMasterSlave.setText(stringForTime(duration));
        if (mCurrentTimeMasterSlave != null)
            mCurrentTimeMasterSlave.setText(stringForTime(position));

        return position;
    }
/*
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.zoom:
                //archive(item);
                return true;

            default:
                mediaPlayerControl.inputButtonPressed();
                return false;
        }
    }*/



    @Override
    public boolean onTouchEvent(MotionEvent event) {
        show(sDefaultTimeout);
        return true;
    }

    @Override
    public boolean onTrackballEvent(MotionEvent ev) {
        show(sDefaultTimeout);
        return false;
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent motionEvent)
    {
        return super.onGenericMotionEvent(motionEvent);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        //Navigation auf dem Mediacontroller
       /* if(event.getAction() == KeyEvent.ACTION_UP)
        {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    moveS

                    return true;
                case KeyEvent.KEYCODE_DPAD_LEFT:

                    return true;
            }
        }*/

        //Mediaplayer Action
        if (mediaPlayerControl == null) {
            return true;
        }

        int keyCode = event.getKeyCode();
        final boolean uniqueDown = event.getRepeatCount() == 0
                && event.getAction() == KeyEvent.ACTION_DOWN;

        if (keyCode ==  KeyEvent.KEYCODE_HEADSETHOOK
                || keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE
                || keyCode == KeyEvent.KEYCODE_SPACE)
        {
            if (uniqueDown) {
                doPauseResume();
                show(sDefaultTimeout);
                /*if (btnPlayPauseMaster != null) {
                    btnPlayPauseMaster.requestFocus();
                }*/
            }
            return true;
        }
        else if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY) {
            if (uniqueDown && !mediaPlayerControl.isPlaying()) {
                mediaPlayerControl.start();
                updatePausePlay();
                show(sDefaultTimeout);
            }
            return true;
        }
        else if (keyCode == KeyEvent.KEYCODE_MEDIA_STOP
                || keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE) {
            if (uniqueDown && mediaPlayerControl.isPlaying()) {
                mediaPlayerControl.pause();
                updatePausePlay();
                show(sDefaultTimeout);
            }
            return true;
        }
        else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN
                || keyCode == KeyEvent.KEYCODE_VOLUME_UP
                || keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
            // don't show the controls for volume adjustment
            return super.dispatchKeyEvent(event);
        }
        else if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_MENU)
        {
            if (uniqueDown) {
                hide();
            }
            return true;
        }

        show(sDefaultTimeout);
        return super.dispatchKeyEvent(event);
    }

    private View.OnClickListener mPauseListener = new View.OnClickListener() {
        public void onClick(View v) {
            doPauseResume();
            show(sDefaultTimeout);
        }
    };

    /*

    private View.OnClickListener btnZoomXPListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mediaPlayerControl.zoom(1,0);
        }
    };

    private View.OnClickListener btnZoomYPListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mediaPlayerControl.zoom(0,1);
        }
    };

    private View.OnClickListener btnZoomXMListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mediaPlayerControl.zoom(-1,0);
        }
    };

    private View.OnClickListener btnZoomYMListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mediaPlayerControl.zoom(0,-1);
        }
    };*/

    public void updatePausePlay() {
        if (mRoot == null || btnPlayPause == null || mediaPlayerControl == null) {
            return;
        }
        if (!mediaPlayerControl.isPlaying()) {
            if(btnPlayPause.isFocused())
            {
                btnPlayPause.setImageResource(R.drawable.ic_play_arrow_black_24dpfocus);
            }
            else
            {
                btnPlayPause.setImageResource(R.drawable.ic_play_arrow_black_24dp);
            }

        } else {
            if(btnPlayPause.isFocused()) {
                btnPlayPause.setImageResource(R.drawable.ic_pause_circle_outline_black_24dpfocus);
            }
            else
            {
                btnPlayPause.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
            }
        }
    }

    public void doPauseResume() {
        if (mediaPlayerControl == null) {
            return;
        }

        if (mediaPlayerControl.isPlaying()) {
            mediaPlayerControl.pause();
        } else {
            mediaPlayerControl.start();
        }
        updatePausePlay();
    }

    // There are two scenarios that can trigger the seekbar listener to trigger:
    //
    // The first is the user using the touchpad to adjust the posititon of the
    // seekbar's thumb. In this case onStartTrackingTouch is called followed by
    // a number of onProgressChanged notifications, concluded by onStopTrackingTouch.
    // We're setting the field "mDragging" to true for the duration of the dragging
    // session to avoid jumps in the position in case of ongoing playback.
    //
    // The second scenario involves the user operating the scroll ball, in this
    // case there WON'T BE onStartTrackingTouch/onStopTrackingTouch notifications,
    // we will simply apply the updated position without suspending regular updates.
    private OnSeekBarChangeListener mSeekListener = new OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar bar) {
            show(3600000);

            mDragging = true;

            // By removing these pending progress messages we make sure
            // that a) we won't update the progress while the user adjusts
            // the seekbar and b) once the user is done dragging the thumb
            // we will post one of these messages to the queue again and
            // this ensures that there will be exactly one message queued up.
            mHandler.removeMessages(SHOW_PROGRESS);
        }

        public void onProgressChanged(SeekBar bar, int progress, boolean fromuser) {
            if (mediaPlayerControl == null) {
                return;
            }

            if (!fromuser) {
                // We're not interested in programmatically generated changes to
                // the progress bar's position.
                return;
            }

            long duration = mediaPlayerControl.getDuration();
            long newposition = (duration * progress) / 1000L;
            mediaPlayerControl.seekTo((int) newposition);
            if (mCurrentTimeMasterSlave != null)
                mCurrentTimeMasterSlave.setText(stringForTime( (int) newposition));
        }

        public void onStopTrackingTouch(SeekBar bar) {
            mDragging = false;
            setProgress();
            updatePausePlay();
            show(sDefaultTimeout);

            // Ensure that progress is properly updated in the future,
            // the call to show() does not guarantee this because it is a
            // no-op if we are already showing.
            mHandler.sendEmptyMessage(SHOW_PROGRESS);
        }
    };

    @Override
    public void setEnabled(boolean enabled) {
        if (btnPlayPause != null) {
            btnPlayPause.setEnabled(enabled);
        }

        if (seekBarMasterSlave != null) {
            seekBarMasterSlave.setEnabled(enabled);
        }
        //disableUnsupportedButtons();
        super.setEnabled(enabled);
    }




    public interface MediaPlayerControl {
        void showCalibrateDialog();
        void showMenu();
        void recenterVideo();
        void ZoomDefault();
        void Zoom360();
        void    showZoomDialog();
        void    zoom(int x, int y);
        void    start();
        void    pause();
        int     getDuration();
        int     getCurrentPosition();
        void    seekTo(int pos);
        boolean isPlaying();
        int     getBufferPercentage();
        boolean canPause();
        void    setOnCompletionListener();
        void    openFileActivity();
        void    stop();
        boolean isPlayerInit();
    }

    private static class MessageHandler extends Handler {
        private final WeakReference<VideoControllerView> mView;

        MessageHandler(VideoControllerView view) {
            mView = new WeakReference<>(view);
        }
        @Override
        public void handleMessage(Message msg) {
            VideoControllerView view = mView.get();
            if (view == null || view.mediaPlayerControl == null) {
                return;
            }

            int pos;
            switch (msg.what) {
                case FADE_OUT:

                    view.hide();
                    break;
                case SHOW_PROGRESS:
                    pos = view.setProgress();
                    if (!view.mDragging && view.mShowing && view.mediaPlayerControl.isPlaying()) {
                        msg = obtainMessage(SHOW_PROGRESS);
                        sendMessageDelayed(msg, 1000 - (pos % 1000));
                    }
                    break;
            }
        }
    }

}