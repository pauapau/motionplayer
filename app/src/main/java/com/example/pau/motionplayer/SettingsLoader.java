package com.example.pau.motionplayer;

import android.content.SharedPreferences;

/**
 * Created by oko on 17.03.2016.
 */
public class SettingsLoader {

    SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public SettingsLoader(SharedPreferences sharedPreferences)
    {
        this.sharedPreferences=sharedPreferences;
        this.editor=sharedPreferences.edit();
    }

    public int loadX()
    {
        return sharedPreferences.getInt("x", 0);

    }

    public void saveDefaultVideoLocation(String defaultVideoLocation)
    {
        editor.putString("defaultVideoLocation", defaultVideoLocation);
        editor.commit();
    }

    public String loadDefaultVideoLocation()
    {
        return sharedPreferences.getString("defaultVideoLocation", "/sdcard");
    }

    public int loadY()
    {
        return  sharedPreferences.getInt("y", 0);
    }

    public void saveBegrenzungXY(int x, int y)
    {
        if(x!=0 || y!=0) {
            editor.putInt("x", x);
            editor.putInt("y", y);
            editor.commit();
        }
    }
}
